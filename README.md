Case Study
=========


Quick start:
=========
```
http://54.183.252.197/
http://54.153.91.251/
 or
https://54.183.252.197/
https://54.153.91.251/
```
* Refresh pages to see page is serves by both hosts (round-robin).
* 'static' page has links to CGI pages. 

Access:
=========
```
ssh ec2-user@54.183.252.197 -i aws.pem
ssh ec2-user@54.153.91.251  -i aws.pem
```

use this private key:
 /root/.ssh/aws.pem  (make sure permission are 0600)
 
```
-----BEGIN RSA PRIVATE KEY-----
mIIEowIBAAKCAQEAjWaQvtVDQpPRJ7W7IZmXDnE+7wV6ixt3vGhHFxDQoz8OoWeHPWJX3TC79GBU
EtKF5yY2U+L3dnsFeVfwPAB4wAd3mNFU4xbMKhn0bkrVsGmuRb/Ls7g4tlLyYOQi7qd6u6q3FvgM
JXDUPW09RLBZik3Q7Denn2Fg+D9qwaqhN5CUr2eC+XDzgzUPBcFHM+AW4PWECbiSqd4xEk12EG6z
UoTMIaTLtR8KExa85GRi/ZeD07TujclhrxhN20s2+W4l/KRRdR+VKI65di9dtlF6Ve6A7E0HlH37
1G9KLPSbQxuJ+as2/5dkBMWobuJpyCYD8DtKnMQwygBTbMmb8Z6YVQIDAQABAoIBAGJf2BYcUyYP
pvUEl0OCErjmis1KN35uRrhaqBLhDPZ2GVyrpUhMkip+DFWxauyJcMVnLWbhQh846Y8JYkasYVXg
HBHCmVdAdhxYYFkXUWHhtHAsRrErWrFmNeC496Cn/lhwJd8iBXlK7S4rQPP36OOyzcZ51fGpkwhD
PqesaWs47yCv/IKwazrnkfS6Vtf29fd3nagjHxe+YLkMlJvCbExJjd3RvYz1aiRKpDdSRXR8AddK
+TXeEQxsSVRPmVbk6MUOPYwktJPTzvzy4FRlKdgsIP/9hBB/nweFukqTJ7njCeR6DmDoFLSwotqk
6r+Eb1OtVW07FNbcmgxNqMvXXsECgYEAyt6fop2I0YQ0QggV88cvXMoMxxj5swFTI8tumGYZL/mD
p2/cshJhnD6upyyrJI5Zfse0HtNxyghLd10sFmNnjitzQKGtKG7RUN7jxsHL+csJEJOy2TT2v0kK
X35o97+L8BAfP3jr27+zMkgAYlk6eH3pfcaCixaeKdA5ovgr5gUCgYEAsm7CcigOTTizTXDylXTv
CeereGR8vMZ6VC0evXUz0bllymuFqO878YGTKcduQBBs0OV1qpUPL7L82DI3sA9fsZ3eOrnXvh4+
3dlGF2OGptrk2S00KDJsA4KUoB4YR0a0fw9lOJPeFUtWDaIy45ev4mIYWEd791xKyJ0HsXUxqhEC
gYAsn+vdiWmfLMWSANVTWm1Aq/FDmmrSxWfUZ9tcCplfxDMBkcmqIgD2rZMdkuXSlL169dLZNQnC
tzBigUxBYHxW+ZfQj7/XNQk2ENFp1xdXSyIjU1n0DYJ5cUem2dedJ7VQiTtWavL206RHWy86cwY5
mvjRgekVT1RbPkJvZsfBQQKBgFSwoXYRcQhR+zisfVdW0wY+gXJ5L4NiKTm+2LJP6LKIbrnu4tfW
BPuR5smTsM6TfTKBVqIKcdTP/ZaX7zqPcXoaVhpFjxLbxDcEI3KCeKzFhI1YGUNi+0WHOwfsgNWD
x7PvGLoL5QLyaPu7mq/PlVm7+ZYdB8fWdGBXuAkQeOwhAoGBAMdDGXonVaQzz7ugRjDXrbfZMRj0
ALFiajRZ5Obhmo9YTOw8VJ0H0ultNbVKP5IewHQOzsy8RUPOOK0Ukbvqmu+dCh6QDGRlvXcKxrFM
uK0SyFXAMcBRcO8hIY1vC479acd4ul8I2NOtFnC8U5dcFPoal8SF9Tb7tqqLMTe47PwFZr
-----END RSA PRIVATE KEY-----
```

Build:
=========
`$ ansible-playbook fc.yml`

(Re)buid specific role:
```
	$ ansible-playbook fc.yml -t frontend
	$ ansible-playbook fc.yml -t backtend
```
	
Architecture:
=========
```
2 x Nginx
2 x Apache
1 x MySQL (54.183.252.197)
```

Security:
=========
```
Type   Protocol  Port    Range Source
SSH    TCP       22      0.0.0.0/0         (public)
HTTP   TCP       80      0.0.0.0/0         (public)
HTTPS  TCP       443     0.0.0.0/0         (public)
MYSQL  TCP       3306    172.31.2.97/32    (allow access from second host)
Custom TCP       8080    172.31.2.96/32    (allow cross host nginx redirect)
Custom TCP       8080    172.31.2.97/32    (allow cross host nginx redirect)
```

Detail:
=========
(see diagram case_study_FC.pdf)

Frontend (x2) hosts runs Apache on port 8080 (blocked from public) and serves one static page and one CGI. 
CGI pulls a single field from MySQL database. Both frontend hosts also run nginx for load balancing, redundency and SSL termination. Both hosts will accept 
connections and will reroute to (in this case) the same two hosts on port 8080 (where apache listens).

Backend consists of a single MySQL instance, Ansible is pre-populating data (mysqldump) into the table and 
sets privileges.

Everything is applied and configured through Ansible, nothing has to be done manually on the remote hosts!



	
	
