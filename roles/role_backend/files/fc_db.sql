
DROP TABLE IF EXISTS `data`;

CREATE TABLE `data` (
  `my_key` char(60) DEFAULT NULL,
  `my_value` char(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


LOCK TABLES `data` WRITE;

INSERT INTO `data` VALUES ('hello','zohar');

UNLOCK TABLES;
