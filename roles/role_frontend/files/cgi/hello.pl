#!/usr/bin/perl
#
use Data::Dumper;



#--------[ CONNECT TO SQL ]---------#
$db_ip = "172.31.2.96";
$db_database = "fc_db";
$db_user = "fc_user";
$db_passwd = "fc_pass";

use DBI;
$dsn = "dbi:mysql:$db_database:$db_ip:3306";
$dbh = DBI->connect($dsn, $db_user, $db_passwd);


#--------[ main ]---------#
print "Content-type: text/html\n\n";
print "<html>\n";

my $value = get_value($dbh,"hello");
print "data: $value<br>\n";
print "<pre>\n";
print "--------------------\n";
print "Public IP (nginx):   $ENV{'HTTP_HOST'}\n";
print "Private IP:          $ENV{'SERVER_ADDR'}\n";
print "Your IP (nginx):     $ENV{'REMOTE_ADDR'}\n";
#print Dumper(\%ENV);
print "</pre>\n";
print "</html><br>\n";
exit 0;


#--------[ subs ]---------#
sub get_value{
  my $dbh = shift;
  my $key = shift;

  $sth = $dbh->prepare("select my_value from fc_db.data where my_key='$key'");
  $sth->execute();
  my ($value) = $sth->fetchrow;
  return $value;

}
